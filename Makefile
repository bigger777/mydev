CC := gcc

KMOD_DIR := kmod

TESTS_DIR := tests
TESTS_SRC := tests.c
TESTS_BIN := tests

all: module_build module_insert tests_run module_delete clean check

module_build:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD)/$(KMOD_DIR) modules

module_insert:
	sudo insmod $(KMOD_DIR)/mydev.ko

module_delete:
	sudo rmmod mydev

tests_build: $(TESTS_DIR)
	$(CC) $(TESTS_DIR)/$(TESTS_SRC) -o $(TESTS_DIR)/$(TESTS_BIN)

tests_run: tests_build
	./$(TESTS_DIR)/$(TESTS_BIN).sh
	./$(TESTS_DIR)/$(TESTS_BIN)

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD)/$(KMOD_DIR) clean
	rm -rf $(TESTS_DIR)/$(TESTS_BIN)

check:
	dmesg | grep mydev
