#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>

#include "../kmod/mydev_ioctl.h"

#define MYDEVICE "/dev/mydev"

#define TEST_SUCCESS() printf("<%s> Success\n", __func__)
#define TEST_FAIL(__fmt__, ...) printf("<%s> Failed: " __fmt__ "\n", __func__, ##__VA_ARGS__)

static const char test_str1[] = "test string1";
static const char test_str2[] = "test string2";
static const char test_str3[] = "test string3";

void test1(void)
{
    int fd = open(MYDEVICE, O_RDWR);
    if (fd == -1)
    {
        TEST_FAIL("open failed: %s", strerror(errno));
        goto error;
    }

    int ret = ioctl(fd, MYDEV_IOCTL_SET_MODE_MULTI, NULL);
    if (ret)
    {
        TEST_FAIL("ioctl failed: %d", ret);
        goto error;
    }

    ret = ioctl(fd, MYDEV_IOCTL_SET_MODE_SINGLE, NULL);
    if (ret)
    {
        TEST_FAIL("ioctl failed: %d", ret);
        goto error;
    }

    ret = ioctl(fd, MYDEV_IOCTL_SET_MODE_MULTI, NULL);
    if (ret)
    {
        TEST_FAIL("ioctl failed: %d", ret);
        goto error;
    }

    ret = ioctl(fd, MYDEV_IOCTL_SET_MODE_MULTI, NULL);
    if (ret)
    {
        TEST_FAIL("ioctl failed: %d", ret);
        goto error;
    }

    ret = ioctl(fd, MYDEV_IOCTL_SET_MODE_DEFAULT, NULL);
    if (ret)
    {
        TEST_FAIL("ioctl failed: %d", ret);
        goto error;
    }

    TEST_SUCCESS();

error:

    close(fd);
}

void test2(void)
{
    int fd1 = open(MYDEVICE, O_RDWR);
    if (fd1 == -1)
    {
        TEST_FAIL("open failed: %s", strerror(errno));
        return;
    }

    int ret = ioctl(fd1, MYDEV_IOCTL_SET_MODE_MULTI, NULL);
    if (ret)
    {
        TEST_FAIL("ioctl failed: %d", ret);
        goto error1;
    }

    write(fd1, test_str1, sizeof(test_str1));

    int fd2 = open(MYDEVICE, O_RDWR);
    if (fd2 == -1)
    {
        TEST_FAIL("open failed: %s", strerror(errno));
        goto error1;
    }

    write(fd2, test_str2, sizeof(test_str2));

    int fd3 = open(MYDEVICE, O_RDWR);
    if (fd3 == -1)
    {
        TEST_FAIL("open failed: %s", strerror(errno));
        goto error2;
    }

    write(fd3, test_str3, sizeof(test_str3));

    char buf1[sizeof(test_str1)] = { 0 };
    char buf2[sizeof(test_str2)] = { 0 };
    char buf3[sizeof(test_str3)] = { 0 };

    read(fd1, buf1, sizeof(buf1));
    read(fd2, buf2, sizeof(buf2));
    read(fd3, buf3, sizeof(buf3));

    if (strncmp(buf1, test_str1, sizeof(buf1)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf1, test_str1);
        goto error3;
    }

    if (strncmp(buf2, test_str2, sizeof(buf2)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf2, test_str2);
        goto error3;
    }

    if (strncmp(buf3, test_str3, sizeof(buf3)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf3, test_str3);
        goto error3;
    }

    TEST_SUCCESS();

error3:
    close(fd3);
error2:
    close(fd2);
error1:
    close(fd1);
}

void test3(void)
{
    int fd = open(MYDEVICE, O_RDWR);
    if (fd == -1)
    {
        TEST_FAIL("open failed: %s", strerror(errno));
        return;
    }

    int ret = ioctl(fd, MYDEV_IOCTL_SET_MODE_SINGLE, NULL);
    if (ret)
    {
        TEST_FAIL("ioctl failed: %d", ret);
        goto error;
    }

    int fd2 = open(MYDEVICE, O_RDWR);
    if (fd2 != -1)
    {
        TEST_FAIL("Can't open another device at SINGLE modes");
        close(fd2);
        goto error;
    }

    write(fd, test_str1, sizeof(test_str1));
    write(fd, test_str2, sizeof(test_str2));
    write(fd, test_str3, sizeof(test_str3));

    char buf[sizeof(test_str1)] = { 0 };

    read(fd, buf, sizeof(buf));

    if (strncmp(buf, test_str1, sizeof(buf)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf, test_str1);
        goto error;
    }

    memset(buf, 0, sizeof(buf));

    read(fd, buf, sizeof(buf));

    if (strncmp(buf, test_str2, sizeof(buf)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf, test_str2);
        goto error;
    }

    memset(buf, 0, sizeof(buf));

    read(fd, buf, sizeof(buf));

    if (strncmp(buf, test_str3, sizeof(buf)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf, test_str3);
        goto error;
    }

    TEST_SUCCESS();

error:
    close(fd);
}

void test4(void)
{
    int fd = open(MYDEVICE, O_RDWR);
    if (fd == -1)
    {
        TEST_FAIL("open failed: %s", strerror(errno));
        return;
    }

    write(fd, test_str1, sizeof(test_str1));

    close(fd);

    fd = open(MYDEVICE, O_RDWR);
    if (fd == -1)
    {
        TEST_FAIL("open failed: %s", strerror(errno));
        return;
    }

    write(fd, test_str2, sizeof(test_str2));

    close(fd);

    fd = open(MYDEVICE, O_RDWR);
    if (fd == -1)
    {
        TEST_FAIL("open failed: %s", strerror(errno));
        return;
    }

    write(fd, test_str3, sizeof(test_str3));

    char buf[sizeof(test_str1)] = { 0 };

    read(fd, buf, sizeof(buf));

    if (strncmp(buf, test_str1, sizeof(buf)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf, test_str1);
        goto error;
    }

    memset(buf, 0, sizeof(buf));

    read(fd, buf, sizeof(buf));

    if (strncmp(buf, test_str2, sizeof(buf)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf, test_str2);
        goto error;
    }

    memset(buf, 0, sizeof(buf));

    read(fd, buf, sizeof(buf));

    if (strncmp(buf, test_str3, sizeof(buf)))
    {
        TEST_FAIL("'%s' and '%s' are different", buf, test_str3);
        goto error;
    }

    TEST_SUCCESS();

error:
    close(fd);
}

int main(int argc, char** argv)
{
    test1(); /* test ioctl */
    test2(); /* test MULTI mode */
    test3(); /* test SINGLE mode */
    test4(); /* test DEFAULT mode */

    return 0;
}
