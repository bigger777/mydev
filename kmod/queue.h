#ifndef _QUEUE_H_
#define _QUEUE_H_

#include "helpers.h"

#define MAX_QUEUE_DEPTH 1000

typedef struct
{
    char* data;
    size_t data_size;
    struct list_head list;
} queue_item_t;

typedef struct
{
    struct list_head head_list;
    size_t depth;
} queue_t;

static inline bool queue_is_full(const queue_t* queue)  { return queue->depth >= MAX_QUEUE_DEPTH; }
static inline bool queue_is_empty(const queue_t* queue) { return queue->depth == 0; }

void queue_init(queue_t* queue);
void queue_cleanup(queue_t* queue);

int queue_push(queue_t* queue, const char* data, const size_t size);
int queue_pop(queue_t* queue, char** data, size_t* size);

queue_t* queue_create_empty(void);

#endif // _QUEUE_H_
