#ifndef _HELPERS_H_
#define _HELPERS_H_

#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#define MYDEV_NAME "mydev"
#define MYDEV_CLASS_NAME "mydev_class"

#define LOG_ERR(__fmt__, ...) \
    printk(KERN_ERR MYDEV_NAME ": " __fmt__ "\n", ##__VA_ARGS__)

#define LOG_INFO(__fmt__, ...) \
    printk(KERN_INFO MYDEV_NAME ": " __fmt__ "\n", ##__VA_ARGS__)

// #define DEBUG

#ifdef DEBUG
#define LOG_TRACE() printk(KERN_DEBUG MYDEV_NAME ": <%s>", __func__)
#else
#define LOG_TRACE(...)
#endif

#endif // _HELPERS_H_
