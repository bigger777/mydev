#include "queue.h"

void queue_init(queue_t* queue)
{
    if (!queue) return;

    INIT_LIST_HEAD(&queue->head_list);

    queue->depth = 0;
}

void queue_cleanup(queue_t* queue)
{
    if (!queue) return;

    struct list_head* pos;
    struct list_head* q;

    list_for_each_safe(pos, q, &queue->head_list)
    {
        queue_item_t* item = list_entry(pos, queue_item_t, list);

        kfree(item->data);

        list_del(pos);

        kfree(item);
    }

    queue->depth = 0;
}

int queue_push(queue_t* queue, const char* data, const size_t size)
{
    if (!queue || !data || !size) return 0;

    if (queue_is_full(queue))
    {
        LOG_ERR("Can't add item, queue is full: %zu", queue->depth);
        return -1;
    }

    queue_item_t* item = kzalloc(sizeof(queue_item_t), GFP_KERNEL);
    if (!item)
    {
        LOG_ERR("kzalloc failed");
        return -ENOMEM;
    }

    item->data_size = size;
    item->data = (char*) data;

    list_add_tail(&item->list, &queue->head_list);

    queue->depth++;

    return 0;
}

int queue_pop(queue_t* queue, char** data, size_t* size)
{
    if (!queue || !data || !size) return 0;

    if (queue_is_empty(queue))
    {
        *size = 0;
        *data = NULL;

        LOG_ERR("Queue is empty");

        return -1;
    }

    queue_item_t* item = list_entry(queue->head_list.next, queue_item_t, list);

    *size = item->data_size;
    *data = item->data;

    list_del(&item->list);

    kfree(item);

    queue->depth--;

    return 0;
}

queue_t* queue_create_empty(void)
{
    queue_t* new_queue = kzalloc(sizeof(queue_t), GFP_KERNEL);
    if (!new_queue)
    {
        LOG_ERR("kzalloc failed");
        return NULL;
    }

    queue_init(new_queue);

    return new_queue;
}
