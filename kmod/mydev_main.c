#include "mydev_main.h"
#include "mydev_ioctl.h"

static queue_t single_queue;
static mydev_t mydev;

static DEFINE_MUTEX(mutex);

static inline const char* mydev_stringize_mode(const mode_t mode)
{
    switch (mode)
    {
        case MYDEV_MODE_DEFAULT: return "DEFAULT";
        case MYDEV_MODE_SINGLE:  return "SINGLE";
        case MYDEV_MODE_MULTI:   return "MULTI";

        default: break;
    }

    return "UNKNOWN";
}

static int mydev_open(struct inode* inode, struct file* file)
{
    mutex_lock(&mutex);

    LOG_TRACE();

    mydev.open_count++;

    switch (mydev.mode)
    {
        case MYDEV_MODE_DEFAULT:
        {
            file->private_data = &single_queue;

            break;
        }
        case MYDEV_MODE_SINGLE:
        {
            if (mydev.open_count > 1)
            {
                mydev.open_count--;
                LOG_ERR("Can't open another device at 'SINGLE' mode");

                mutex_unlock(&mutex);
                return -EBUSY;
            }

            break;
        }
        case MYDEV_MODE_MULTI:
        {
            queue_t* new_queue = queue_create_empty();
            if (!new_queue)
            {
                LOG_ERR("queue_create_empty failed");

                mutex_unlock(&mutex);
                return -1;
            }

            file->private_data = (void*) new_queue;

            break;
        }
        default:
        {
            LOG_ERR("Unknown device mode: %d", mydev.mode);

            mutex_unlock(&mutex);
            return -1;
        }
    }

    LOG_INFO("Device opened at '%s' mode", mydev_stringize_mode(mydev.mode));

    mutex_unlock(&mutex);
    return 0;
}

static int mydev_release(struct inode* inode, struct file* file)
{
    mutex_lock(&mutex);

    LOG_TRACE();

    if (mydev.open_count) mydev.open_count--;

    switch (mydev.mode)
    {
        case MYDEV_MODE_DEFAULT:
        {
            break;
        }
        case MYDEV_MODE_SINGLE:
        {
            queue_cleanup(&single_queue);

            break;
        }
        case MYDEV_MODE_MULTI:
        {
            queue_t* queue = (queue_t*) file->private_data;

            queue_cleanup(queue);
            kfree(queue);

            break;
        }
        default:
        {
            LOG_ERR("Unknown device mode: %d", mydev.mode);

            mutex_unlock(&mutex);
            return -1;
        }
    }

    if (mydev.open_count == 0) mydev.mode = MYDEV_MODE_DEFAULT;

    LOG_INFO("Release device");

    mutex_unlock(&mutex);
    return 0;
}

static ssize_t mydev_read(
        struct file* file,
        char __user* buffer,
        size_t buffer_size,
        loff_t* f_pos)
{
    LOG_TRACE();

    char* data = NULL;
    size_t data_size = 0;

    queue_t* queue = (queue_t*) file->private_data;

    if (!queue_is_empty(queue))
    {
        if (queue_pop(queue, &data, &data_size))
        {
            LOG_ERR("queue_pop failed");
            return -1;
        }
    }
    else
    {
        LOG_INFO("Can't read data from queue: queue is empty");
    }

    if (copy_to_user(buffer, data, data_size))
    {
        LOG_ERR("copy_to_user failed");

        kfree(data);
        return -1;
    }

    kfree(data);

    return data_size;
}

static ssize_t mydev_write(
        struct file* file,
        const char __user* buffer,
        size_t buffer_size,
        loff_t* f_pos)
{
    LOG_TRACE();

    queue_t* queue = (queue_t*) file->private_data;

    if (queue_is_full(queue))
    {
        LOG_ERR("Can't write data to a queue: queue is full");
        return -1;
    }

    char* data = kzalloc(buffer_size, GFP_KERNEL);
    if (!data)
    {
        LOG_ERR("kzalloc failed");
        return -1;
    }

    int ret = copy_from_user(data, buffer, buffer_size);
    if (ret)
    {
        LOG_ERR("copy_from_user failed");
        goto error;
    }

    ret = queue_push(queue, data, buffer_size);
    if (ret)
    {
        LOG_ERR("queue_push failed");
        goto error;
    }

    return buffer_size;

error:

    kfree(data);

    return ret;
}

inline static void mydev_switch_mode(const struct file* file, const mode_t new_mode)
{
    LOG_TRACE();

    queue_t* queue = (queue_t*) file->private_data;

    queue_cleanup(queue);

    switch (mydev.mode)
    {
        case MYDEV_MODE_MULTI:
        {
            kfree(queue);
            break;
        }
        case MYDEV_MODE_DEFAULT:
        case MYDEV_MODE_SINGLE:
        default:
        {
            break;
        }
    }

    mydev.mode = new_mode;

    LOG_INFO("Set device to '%s' mode", mydev_stringize_mode(new_mode));
}

static long mydev_ioctl(struct file* file, unsigned int cmd, unsigned long arg)
{
    mutex_lock(&mutex);

    LOG_TRACE();

    if (mydev.open_count > 1)
    {
        LOG_ERR("Can't change mode: %d devices is opening at '%s' mode",
                mydev.open_count,
                mydev_stringize_mode(mydev.mode));

        mutex_unlock(&mutex);
        return -1;
    }

    switch (cmd)
    {
        case MYDEV_IOCTL_SET_MODE_DEFAULT:
        {
            if (mydev.mode == MYDEV_MODE_DEFAULT)
            {
                mutex_unlock(&mutex);
                return 0;
            }

            mydev_switch_mode(file, MYDEV_MODE_DEFAULT);

            file->private_data = (void*) &single_queue;

            break;
        }
        case MYDEV_IOCTL_SET_MODE_SINGLE:
        {
            if (mydev.mode == MYDEV_MODE_SINGLE)
            {
                mutex_unlock(&mutex);
                return 0;
            }

            mydev_switch_mode(file, MYDEV_MODE_SINGLE);

            file->private_data = (void*) &single_queue;

            break;
        }
        case MYDEV_IOCTL_SET_MODE_MULTI:
        {
            if (mydev.mode == MYDEV_MODE_MULTI)
            {
                mutex_unlock(&mutex);
                return 0;
            }

            mydev_switch_mode(file, MYDEV_MODE_MULTI);

            queue_t* new_queue = queue_create_empty();
            if (!new_queue)
            {
                LOG_ERR("queue_create_empty failed");

                mutex_unlock(&mutex);
                return -1;
            }

            file->private_data = (void*) new_queue;

            break;
        }
        default:
        {
            LOG_ERR("Unknown ioctl command: %u", cmd);

            mutex_unlock(&mutex);
            return -1;
        }
    }

    mutex_unlock(&mutex);
    return 0;
}

static const struct file_operations mydev_fops = {
    .owner = THIS_MODULE,
    .read = mydev_read,
    .write = mydev_write,
    .open = mydev_open,
    .release = mydev_release,
    .unlocked_ioctl = mydev_ioctl
};

static void mydev_cleanup(void)
{
    LOG_TRACE();

    device_unregister(mydev.device);

    class_destroy(mydev.class);

    unregister_chrdev_region(mydev.dev, MINOR_NUM_COUNT);

    LOG_INFO("Device is removed");
}

static char * devnode(struct device * dev, umode_t * mode)
{
    LOG_TRACE();

    if (!mode) return NULL;

    *mode = 0777;

    return NULL;
}


static int mydev_init(void)
{
    LOG_TRACE();

    int ret = alloc_chrdev_region(&mydev.dev, MINOR_NUM, MINOR_NUM_COUNT, MYDEV_NAME);
    if (ret)
    {
        LOG_ERR("alloc_chrdev_region failed: %d", ret);
        return ret;
    }

    cdev_init(&mydev.cdev, &mydev_fops);

    mydev.cdev.owner = THIS_MODULE;
    mydev.cdev.ops = &mydev_fops;

    ret = cdev_add(&mydev.cdev, mydev.dev, 1);
    if (ret)
    {
        LOG_ERR("cdev_add failed: %d", ret);
        goto chrdev_unreg;
    }

    mydev.open_count = 0;

    mydev.mode = MYDEV_MODE_DEFAULT;

    mydev.class = class_create(THIS_MODULE, MYDEV_CLASS_NAME);
    if (IS_ERR(mydev.class))
    {
        LOG_ERR("class_create failed");
        goto class_destroy;
    }

    mydev.class->devnode = devnode;

    mydev.device = device_create(mydev.class, NULL, mydev.dev, NULL, MYDEV_NAME);
    if (IS_ERR(mydev.device))
    {
        LOG_ERR("device_create failed");
        goto error;
    }

    queue_init(&single_queue);

    LOG_INFO("Device is initialized");

    return 0;

error:
    device_unregister(mydev.device);

class_destroy:
    class_destroy(mydev.class);

chrdev_unreg:
    unregister_chrdev_region(mydev.dev, MINOR_NUM_COUNT);

    return ret;
}

MODULE_AUTHOR("@bigger777");
MODULE_LICENSE("GPL");

module_init(mydev_init);
module_exit(mydev_cleanup);
