#ifndef _MYDEV_H_
#define _MYDEV_H_

#include "queue.h"

#define MINOR_NUM 0
#define MINOR_NUM_COUNT 1

typedef enum
{
    MYDEV_MODE_DEFAULT,
    MYDEV_MODE_SINGLE,
    MYDEV_MODE_MULTI,
} mydev_mode_t;

typedef struct
{
    dev_t dev;
    struct cdev cdev;
    struct class * class;
    struct device * device;

    int open_count;
    mydev_mode_t mode;
} mydev_t;

#endif // _MYDEV_H_
